package bigint

import (
	"errors"
	"strings"
)

type Bigint struct {
	Value string
}

var ErrorBadInput = errors.New("bad input, please input only number")

func NewInt(num string) (Bigint, error) {
	allowed := "1234567890"
	var err bool

	if strings.HasPrefix(num, "-") {
		num = strings.Replace(num, "-", "", 1)
	}
	if strings.HasPrefix(num, "0") {
		err = true
	}
	arr := strings.Split(num, "")
	for _, v := range arr {
		if !strings.Contains(allowed, v) {
			err = true
		}
	}

	if err {
		return Bigint{Value: num}, ErrorBadInput
	} else {
		return Bigint{Value: num}, nil

	}
}

func (z *Bigint) Set(num string) error {
	//
	// Validate input
	//
	z.Value = num
	return nil
}

func Add(a, b Bigint) Bigint {
	//
	// MATH ADD
	//
	return Bigint{Value: a.Value + b.Value}
}

func Sub(a, b Bigint) Bigint {
	//
	// MATH SUB
	//
	return Bigint{Value: ""}
}

func Multiply(a, b Bigint) Bigint {
	//
	// MATH Multiply
	//
	return Bigint{Value: ""}
}

func Mod(a, b Bigint) Bigint {
	//
	// MATH Mod
	//
	return Bigint{Value: ""}
}

func (x *Bigint) Abs() Bigint {
	if x.Value[0] == '-' {
		return Bigint{
			Value: x.Value[1:],
		}
	}
	if x.Value[0] == '+' {
		return Bigint{
			Value: x.Value[1:],
		}
	}
	return Bigint{
		Value: x.Value,
	}
}
